namespace CSharpDayFourTesting
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void UniqueNumbersInAListRandomNumbers()
        {
            //Arrange
            CSharpDayFour test = new CSharpDayFour();
            List<int> input = new List<int> { 11, 22, 13, 44, 45, 11, 13, 55, 22 };
            string expected = "11, 22, 13, 44, 45, 55";
            //Act
            string result = test.UniqueElementsOfList(input);
            //Assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void UniqueNumbersInAListAllSameNumbers()
        {
            //Arrange
            CSharpDayFour test = new CSharpDayFour();
            List<int> input = new List<int> { 11, 11, 11, 11, 11, 11, 11, 11, 11 };
            string expected = "11";
            //Act
            string result = test.UniqueElementsOfList(input);
            //Assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void EraseMiddleNumberRandomNumbers()
        {
            //Arrange
            CSharpDayFour test = new CSharpDayFour();
            List<int> input = new List<int> { 11, 12, 13, 14, 15, 16, 17, 18, 19 };
            string expected = "11, 12, 13, 14, 15, 17, 18, 19";
            //Act
            string result = test.RemoveMiddleListElement(input);
            //Assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void EraseMiddleNumberShortList()
        {
            //Arrange
            CSharpDayFour test = new CSharpDayFour();
            List<int> input = new List<int> { 11, 12 };
            string expected = "List is too short!";
            //Act
            string result = test.RemoveMiddleListElement(input);
            //Assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void ExpressionExpansionNestedString()
        {
            //Arrange
            CSharpDayFour test = new CSharpDayFour();
            string input = "AB3(DC)2(F)2(E3(G))";
            string expected = "ABDCDCDCFFEGGGEGGG";
            //Act
            string result = test.ExpressionExpansion(input);
            //Assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void ExpressionExpansionShort()
        {
            //Arrange
            CSharpDayFour test = new CSharpDayFour();
            string input = "HELL3(O)";
            string expected = "HELLOOO";
            //Act
            string result = test.ExpressionExpansion(input);
            //Assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void ShuntingYard()
        {
            //Arrange
            CSharpDayFour test = new CSharpDayFour();
            string testShunting = "3+4*2/(1-5)^2^3";
            string expected = "342*15-23^^/+";
            //Act
            string result = test.ShuntingYard(testShunting);
            //Assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void ShuntingYardNoOperators()
        {
            //Arrange
            CSharpDayFour test = new CSharpDayFour();
            string testShunting = "33";
            string expected = "33";
            //Act
            string result = test.ShuntingYard(testShunting);
            //Assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void ShuntingYardNested()
        {
            //Arrange
            CSharpDayFour test = new CSharpDayFour();
            string testShunting = "34*5/(2-(3+2))";
            string expected = "345*232+-/";
            //Act
            string result = test.ShuntingYard(testShunting);
            //Assert
            Assert.AreEqual(expected, result);
        }
    }
}
﻿using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class CSharpDayFour
{
    static void Main()
    {
        CSharpDayFour test = new CSharpDayFour();
        string input = "HELL3(O)";
        string testShunting = "34*5/(2-(3+2))"; 
        Console.WriteLine(test.ExpressionExpansion(input));
        Console.WriteLine(test.ShuntingYard(testShunting));
    }
    public string UniqueElementsOfList(List<int> list)
    {
        List<int> uniqueElements = new List<int>();
        foreach (int element in list)
        {
            if (!uniqueElements.Contains(element))
            {
                uniqueElements.Add(element);
            }
        }
        return string.Join(", ", uniqueElements);
    }
    public string RemoveMiddleListElement(List<int> list)
    {
        string error = "List is too short!";
        if (list.Count < 3)
        {
            return error;
        }
        List<int> copiedHalf = new List<int>();
        int halfLength;
        if (list.Count % 2 == 0)
        {
            halfLength = list.Count / 2;
        }
        else
        {
            halfLength = (list.Count + 1) / 2;
        }
        list.Remove(list[halfLength]);
        return String.Join(", ", list);
    }
    public string ExpressionExpansion(string expr)
    {
        string newString = "";
        for (int i = 0; i < expr.Length; i++)
        {
            char c = expr[i];
            if (char.IsDigit(c))
            {
                int repeats = c - '0';
                int nested = i + 1;
                while (nested < expr.Length && expr[nested] != ')')
                {
                    nested++;
                }
                string subString = expr.Substring(i + 2, nested - i - 2);
                string expandedString = ExpressionExpansion(subString);
                for (int k = 0; k < repeats; k++)
                {
                    newString += expandedString;
                }
                i = nested;
            }
            else
            {
                if (c != ')')
                    newString += c;
            }
        }
        return newString;
    }
    static bool ShuntingPrecedence(int newPrecedence, char stackedOperator)
    {
        int stackedPrecedence = 0;
        switch (stackedOperator)
        {
            case '(':
                stackedPrecedence = 0;
                break;
            case '^':
                stackedPrecedence = 4;
                if(newPrecedence == 4)
                {
                    return false;
                }
                break;

            case '*':
            case '/':
                stackedPrecedence = 3;
                break;

            case '+':
            case '-':
                stackedPrecedence = 2;
                break;
            default:
                stackedPrecedence = 0;
                break;

        }
        return stackedPrecedence >= newPrecedence;
    }
    public string ShuntingYard(string expr)
    {
        Stack<char> operators = new Stack<char>();
        Queue<char> operands = new Queue<char>();
        for (int i = 0; i < expr.Length; i++)
        {
            if (char.IsDigit(expr[i]) || char.IsLetter(expr[i]))
            {
                operands.Enqueue(expr[i]);
            }
            else
            {
                if (operators.Count == 0)
                {
                    operators.Push(expr[i]);
                }
                else
                {
                    switch (expr[i])
                    {
                        case '(':
                            operators.Push(expr[i]);
                            break;

                        case ')':
                            if (operators.Count > 0)
                            {
                                while (operators.Count > 0 && operators.Peek() != '(')
                                {
                                    char removed = operators.Pop();
                                    operands.Enqueue(removed);
                                }
                            }
                            if(operands.Count > 0)
                            {
                            operators.Pop();
                            }
                            break;
                        case '-':
                            if (operators.Count > 0)
                            {
                                while (operators.Count > 0 && ShuntingPrecedence(2, operators.Peek()))
                                {
                                    char removed = operators.Pop();
                                    operands.Enqueue(removed);
                                }
                            }
                            operators.Push(expr[i]);
                            break;

                        case '+':
                            if (operators.Count > 0)
                            {
                                while (operators.Count > 0 && ShuntingPrecedence(2, operators.Peek()))
                                {
                                    char removed = operators.Pop();
                                    operands.Enqueue(removed);
                                }
                            }
                            operators.Push(expr[i]);
                            break;

                        case '/':
                            if (operators.Count > 0)
                            {
                                while (operators.Count > 0 && ShuntingPrecedence(3, operators.Peek()))
                                {
                                    char removed = operators.Pop();
                                    operands.Enqueue(removed);
                                }
                            }
                            operators.Push(expr[i]);
                            break;

                        case '*':
                            while (operators.Count > 0 && ShuntingPrecedence(3, operators.Peek()))
                            {
                                char removed = operators.Pop();
                                operands.Enqueue(removed);
                            }
                            operators.Push(expr[i]);
                            break;

                        case '^':
                            while (operators.Count > 0 && ShuntingPrecedence(4, operators.Peek()))
                            {
                                char removed = operators.Pop();
                                operands.Enqueue(removed);
                            }
                            operators.Push(expr[i]);
                            break;
                    }
                }
            }
        }
        while (operators.Count > 0)
        {
            char removed = operators.Pop();
            operands.Enqueue(removed);
        }

        char[] resultArray = operands.ToArray();
        return new string(resultArray);
    }
   // public string HanoiTowers(int discsCount)
}

﻿using System;

public class CSharpDayOne
{
    static void Main()
    {

    }
    public bool IsOdd(int number)
    {
        if(number == 0)
        {
            return false;
        }
        return number % 2 != 0;
    }
    public bool isPrime(int number)
    {
        if (number <= 1)
        {
            return false;
        }
        else if (number > 1)
        {
            for (int i = 2; i < number; i++)
            {
                if (number % i == 0 && number != i)
                {
                    return false;
                }
            }
        }
        return true;
    }
    public int Min(int[] array)
    {
        Array.Sort(array);
        return array[0];
    }
    public int KthMin(int k, int[] array)
    {
        for (int n = 0; n < array.Length; n++)
        {
            int Nth = 1;
            int min = array[n];
            for (int i = 1; i < array.Length; i++)
            {
                if (min < array[i])
                {
                    Nth++;
                }
            }
            if (Nth == k)
            {
                return min;
            }
        }
        return 0;
    }
    public int GetOddOccurrence(int num, int[] array)
    {
        int odd = 0;
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i] == num)
            {
                odd++;
            }
        }
        if (odd % 2 != 0)
        {
            return odd;
        }
        return 0;
    }
    public int GetAverage(int[] array)
    {
        if(array.Length == 0)
        {
            return 0;
        }
        int sum = 0;
        for (int i = 0; i < array.Length; i++)
        {
            sum += array[i];
        }
        sum /= array.Length;
        return sum;
    }
    public int ApowB(int a, int b)
    {
        if(b == 0){
            return 1;
        }
        int pow = a;
        for (int i = 1; i < b; i++)
        {
            pow *= a;
        }
        return pow;
    }
    public int DoubleFac(int n)
    {
        if(n == 0)
        {
            return 0;
        }
        int result = 1;
        for (int i = 1; i <= n * 2; i++)
        {
            result *= i;
        }
        return result;
    }
    public long KthFac(int k, int n)
    {
        long result = 1;
        for (int i = 1; i <= n * k; i++)
        {
            result *= i;
        }
        return result;
    }
    public long MaximalScalarSum(int[] a, int[] b)
    {
        long sum = 0;
        for (int i = 0; i < a.Length; i++)
        {
            sum += (a[i] * b[i]);
        }
        return sum;
    }
    public bool EqualSumSides(int[] numbers)
    {
        int leftSum = 0;
        int rightSum = 0;
        int halfLength = 0;
        if (numbers.Length % 2 == 0)
        {
            halfLength = numbers.Length / 2;
        }
        else if (numbers.Length % 2 == 1)
        {
            halfLength = (numbers.Length - 1) / 2;
        }
        for (int i = 0; (i < halfLength); i++)
        {
            leftSum += numbers[i];
            rightSum += numbers[(numbers.Length) - i];
        }
        if (leftSum > rightSum && leftSum - numbers[halfLength] == rightSum)
        {
            return true;
        }
        else if (leftSum < rightSum && rightSum - numbers[halfLength] == leftSum)
        {
            return true;
        }
        return false;
    }
    public String Reverse(String argument)
    {
        string reveredString = "";
        for (int i = 0; i < argument.Length - 1; i++)
        {
            reveredString += argument[argument.Length - i];
        }
        return reveredString;
    }
    public String ReverseEveryWord(String arg)
    {
        string reversedString = "";
        string[] words = arg.Split(" ");
        for (int i = 0; i < words.Length - 1; i++)
        {
            for (int m = 0; m < words[i].Length - 1; m++)
            {
                reversedString += words[i][m];
            }
        }
        return reversedString;
    }
    public bool IsPalindrome(String argument)
    {
        int checkAll = 0;
        for (int i = 0; i < argument.Length - 1; i++)
        {
            if (argument[i] == argument[argument.Length - (1 + i)])
            {
                checkAll++;
                if (i == argument.Length - (1 + i))
                {
                    return true;
                }
            }
        }
        return false;
    }
    public bool IsPalindrome(int argument)
    {
        int checkAll = 0;
        string numStr = argument.ToString();
        for (int i = 0; i < numStr.Length - 1; i++)
        {
            if (numStr[i] == numStr[numStr.Length - (1 + i)])
            {
                checkAll++;
                if (i == numStr.Length - (1 + i))
                {
                    return true;
                }
            }
        }
        return false;
    }
    public String CopyChars(String input, int k)
    {
        string newString = "";
        for (int i = 0; i < k; i++)
        {
            newString += input;
        }
        return newString;
    }
}

namespace CSharpDayOneTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestingIsNumberOddWithOddNumber()
        {
            //Arrange
            int test = 3;
            CSharpDayOne tester = new CSharpDayOne();
            //Act
            bool result=tester.IsOdd(test);
            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void TestingIsNumberOddWithNonOddNumber()
        {
            //Arrange
            int test = 4;
            CSharpDayOne tester = new CSharpDayOne();
            //Act
            bool result = tester.IsOdd(test);
            //Assert
            Assert.IsFalse(result);
        }
        [TestMethod]
        public void TestingIsNumberOddWithZero()
        {
            //Arrange
            int test = 0;
            CSharpDayOne tester = new CSharpDayOne();
            //Act
            bool result = tester.IsOdd(test);
            //Assert
            Assert.IsFalse(result);
        }
        [TestMethod]
        public void FindMinInArrayRandomNumbers()
        {
            //Arrange
            int[] test ={1,22,13,0,14,53,22};
            CSharpDayOne tester = new CSharpDayOne();
            int expected = 0;
            //Act
            int result = tester.Min(test);
            //Assert
            Assert.AreEqual(expected,result);
        }
        [TestMethod]
        public void GetAverageValueInArrayRandomNumbers()
        {
            //Arrange
            int[] test = { 7, 13, 10, 0, 5, 15, 20 };
            CSharpDayOne tester = new CSharpDayOne();
            int expected = 10;
            //Act
            int result = tester.GetAverage(test);
            //Assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void GetAverageValueInArrayNulls()
        {
            //Arrange
            int[] test = { 0, 0, 0, 0, 0, 0, 0 };
            CSharpDayOne tester = new CSharpDayOne();
            int expected = 0;
            //Act
            int result = tester.GetAverage(test);
            //Assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void ApowBwithRandomNumbers()
        {
            //Arrange
            int testA=4;
            int testB = 2;
            CSharpDayOne tester = new CSharpDayOne();
            int expected = 16;
            //Act
            int result = tester.ApowB(testA,testB);
            //Assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void ApowBRaisedToZero()
        {
            //Arrange
            int testA = 4;
            int testB = 0;
            CSharpDayOne tester = new CSharpDayOne();
            int expected = 1;
            //Act
            int result = tester.ApowB(testA, testB);
            //Assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void DoubleFactorial()
        {
            //Arrange
            int test = 2;
            CSharpDayOne tester = new CSharpDayOne();
            int expected = 24;
            //Act
            int result = tester.DoubleFac(test);
            //Assert
            Assert.AreEqual(expected, result);
        }
    }
}
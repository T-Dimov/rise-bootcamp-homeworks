namespace TestingCSharpDayThree
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void MissingnumberRandomNumbers()
        {
            //Arrange
            int[] testArray = { 121, 122, 123, 125, 126, 127, 128 };
            CSharpDayThree tested = new CSharpDayThree();
            int expected = 124;
            //Act
            int result = tested.FindMissingNumber(testArray);
            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void MissingNumberSingleNumber()
        {
            //Arrange
            int[] testArray = { 121 };
            CSharpDayThree tested = new CSharpDayThree();
            int expected = 0;
            //Act
            int result = tested.FindMissingNumber(testArray);
            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void MissingNumberTwoNumbers()
        {
            //Arrange
            int[] testArray = { 12,14 };
            CSharpDayThree tested = new CSharpDayThree();
            int expected = 13;
            //Act
            int result = tested.FindMissingNumber(testArray);
            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void MissingNumberEmptyArray()
        {
            //Arrange
            int[] testArray = { };
            CSharpDayThree tested = new CSharpDayThree();
            int expected = 0;
            //Act
            int result = tested.FindMissingNumber(testArray);
            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestingCubedRootRandomNumber()
        {
            //Arrange
            int testInt = 125;
            CSharpDayThree tested = new CSharpDayThree();
            int expected = 5;
            //Act
            int result = tested.CubedRootOfInt(testInt);
            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestingCubedRootTwoDigitNumber()
        {
            //Arrange
            int testInt = 1331;
            CSharpDayThree tested = new CSharpDayThree();
            int expected = 11;
            //Act
            int result = tested.CubedRootOfInt(testInt);
            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestingCubedRootMaximumNonFloatingInteger()
        {
            //Arrange
            int testInt = 2146689000;
            CSharpDayThree tested = new CSharpDayThree();
            int expected = 1290;
            //Act
            int result = tested.CubedRootOfInt(testInt);
            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestingCubedRootGreaterThanPossible()
        {
            //Arrange
            int testInt = 2146689001;
            CSharpDayThree tested = new CSharpDayThree();
            //Assert 
            Assert.ThrowsException<ArgumentException>(()=>tested.CubedRootOfInt(testInt));
        }
    }
}